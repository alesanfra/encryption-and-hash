//
//  lib.c
//  sec-lab2
//
//  Created by Alessio Sanfratello on 22/03/14.
//
//

#include "lib.h"

/* stampa un byte in esadecimale */
void printbyte(char b) {
	char c;
	c = b;
	c = c >> 4;
	c = c & 15; printf("%X", c); c = b;
	c = c & 15; printf("%X:", c);
}

void
my_encrypt(struct crypto *msg)
{
    int nc; /* amount of bytes [de]crypted at each step */
	int nctot; /* total amount of encrypted bytes */
	int i; /* index */
	int ct_ptr; /* first available entry in the buffer */
    
    /* Context allocation */
	EVP_CIPHER_CTX *ctx = (EVP_CIPHER_CTX *) calloc(1,sizeof(EVP_CIPHER_CTX));
	
	/* Context initialization */
	EVP_CIPHER_CTX_init(ctx);
	
	/* Context setup for encryption */
	EVP_EncryptInit(ctx, EVP_des_ecb(), NULL, NULL);
    
	/* Output of the encryption key size */
	printf("Key size %d\n", EVP_CIPHER_key_length(EVP_des_ecb()));
	
	/* Output of the block size */
	printf("Block size %d\n\n", EVP_CIPHER_CTX_block_size(ctx));
    
    
    
    /* Encryption key set up */
	EVP_EncryptInit(ctx, NULL, (unsigned char *) msg->k, NULL);
	
	/* Buffer allocation for the encrypted text */
    msg->ct_len = msg->pt_len + EVP_CIPHER_CTX_block_size(ctx);
	msg->ct = (char *) calloc(msg->ct_len,sizeof(char));
    
    
    /* Encryption */
	nc = 0;
	nctot = 0;
	ct_ptr = 0;
	EVP_EncryptUpdate(ctx, (unsigned char *) msg->ct, &nc, (unsigned char *) msg->pt, msg->pt_len);
	ct_ptr += nc;
	nctot += nc;
	EVP_EncryptFinal(ctx, (unsigned char *) &msg->ct[ct_ptr], &nc);
	nctot += nc;
    
    msg->ct_len = (uint32_t) nctot;
    //realloc(msg->ct,msg->ct_len);
    
	printf("Message size %d\n", msg->pt_len);
	printf("Ciphertext size %d\n", msg->ct_len);
	printf("\nCiphertext:\n");
	
	for (i = 0; i < msg->ct_len; i++)
		printbyte(msg->ct[i]);
	printf("\n\n");
    
    //deallocazione del context
    free(ctx);
}

void
my_decrypt(struct crypto *msg)
{
    int nc; /* amount of bytes [de]crypted at each step */
	int nctot; /* total amount of encrypted bytes */
	//int i; /* index */
	int ct_ptr; /* first available entry in the buffer */
    
    /* Context allocation */
	EVP_CIPHER_CTX *ctx = (EVP_CIPHER_CTX *) calloc(1,sizeof(EVP_CIPHER_CTX));
    
    /* Decryption context initialization */
	EVP_CIPHER_CTX_init(ctx);
	EVP_DecryptInit(ctx, EVP_des_ecb(), (unsigned char *) msg->k, NULL);
    
	msg->pt_len = msg->ct_len + EVP_CIPHER_CTX_block_size(ctx);
	msg->pt = (char *) calloc(msg->pt_len,sizeof(char));
    
    /* Decryption */
	nc = 0;
    nctot = 0;
	ct_ptr = 0;
	EVP_DecryptUpdate(ctx, (unsigned char *) msg->pt, &nc, (unsigned char *) msg->ct, msg->ct_len);
	ct_ptr += nc;
	nctot += nc;
	EVP_DecryptFinal(ctx, (unsigned char *) &msg->pt[ct_ptr], &nc);
	nctot += nc;
    
    msg->pt_len = (uint32_t) nctot;
    
	/* for (i = 0; i < msg->pt_len - 1; i++)
		printf("%c:", msg->pt[i]);
    
	printf("%c\n", msg->pt[msg->pt_len - 1]); */
    
    //deallocazione del context
    free(ctx);
}



void
hash_sha1(struct crypto *msg, char digest[])
{
    unsigned int digest_len = 20, i = 0;
    EVP_MD_CTX* mdctx;
    
    OpenSSL_add_all_digests();
    const EVP_MD* md = EVP_get_digestbyname("sha1");
    mdctx = (EVP_MD_CTX *) calloc(1,sizeof(EVP_MD_CTX));
    
    
    EVP_MD_CTX_init(mdctx);
    EVP_DigestInit(mdctx, md);
    
    
    EVP_DigestUpdate(mdctx,msg->pt,msg->pt_len);
    EVP_DigestFinal_ex(mdctx,(unsigned char *) digest,&digest_len);
    
    printf("\nDigest SHA-1:\n");
	
	for (i = 0; i < digest_len ; i++)
		printbyte(digest[i]);
	printf("\n\n");
    
    //Deallocazione strutture dati
    EVP_MD_CTX_cleanup(mdctx);
    free(mdctx);
}

void
send_and_close(char *msg, uint32_t msg_len, char *ip, unsigned int porta)
{
	struct sockaddr_in srv_addr;
	int ret, sk;
    
    //inserimento indirizzo IP e porta
    //printf("Inserisci indirizzo IP e porta: ");
    //scanf("%s %i",ip,&porta);
    
    //creazione del socket TCP
    sk = socket(AF_INET, SOCK_STREAM, 0);
    
    //azzeramento della struttura
    memset(&srv_addr, 0, sizeof(srv_addr));
    
    //inserimento nella struttura dell'Ip e porta
    srv_addr.sin_family = AF_INET;
    srv_addr.sin_port = htons(porta);
    ret = inet_pton(AF_INET,ip, &srv_addr.sin_addr);
    
    //connessione al server
    ret = connect(sk, (SA *) &srv_addr, sizeof(srv_addr));
    
    //controllo connessione
    if(ret == -1)
    {
        perror("\nErrore di connessione");
        exit(EXIT_FAILURE);
    }
    
    
    //invio della dimensione del messaggio
    ret = send(sk, (void*) &msg_len, sizeof(uint32_t),0);
    
    //controllo invio richiesta
    if(ret == -1)
    {
        perror("\nErrore di invio dati");
        exit(EXIT_FAILURE);
    }
    
    
    
    //invio del messaggio
	ret = send(sk, (void*) msg, msg_len,0);
	
	//controllo invio richiesta
	if(ret == -1)
	{
		perror("\nErrore di invio dati");
		exit(EXIT_FAILURE);
	}
	
	//Stampa informazioni e chiusura connessione
	printf("\nDati inviati al client con successo\n");
    
    close(sk);
}

void
enterPassword(char dest[])
{
    char *buf = calloc(255,sizeof(char));
    printf("\nPassword (no spaces): ");
    scanf("%s",buf);
    stpncpy(dest,buf,8);
    free(buf);
}
