CC = gcc
CFLAGS = -Wall -Wno-deprecated
LIB = -lcrypto

all: server client

clean: clear all

client: client.o lib.o
	$(CC) -o client $^ $(LIB)
	
server: server.o lib.o
	$(CC) -o server $^ $(LIB)

.c.o:
	$(CC) $(CFLAGS) -c -o $@ $<

clear:
	rm -rf *.o server client
