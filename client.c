//
//  client.c
//  sec-lab2
//
//  Created by Alessio Sanfratello on 19/03/14.
//
//

#include "lib.h"

int
main(int argc, char* argv[])
{
    FILE *fp = NULL;
    struct crypto msg;
    int digest_len = 20, i=0;
    
    //Azzero la struttura
    memset(&msg,0,sizeof(msg));
    
    //Inserisco la chiave nella struttura
    //stpncpy(msg.k,"ciaoluca",8);
    enterPassword(msg.k);

    //Controllo degli argomenti
    if(argc != 2)
    {
		printf("Usage: client <file>\n");
		exit(EXIT_FAILURE);
	}

    //Allocazione dello spazio per leggere il contenuto del file
    msg.pt = (char*) calloc(256,sizeof(char));
    
    //Apertura file
    fp = fopen(argv[1],"r");
    
    //Controllo se l'apertura è andata a buon fine
    if(fp == NULL)
    {
        perror("Error while opening the file.\n");
        exit(EXIT_FAILURE);
    }
    
    //Leggo tutti i byte fino a alla fine del file
    for(i=0; (msg.pt[i] = fgetc(fp)) != EOF; i++);
    
    //Fine stringa al posto di EOF
    msg.pt[i] = '\0';
    
    //Chiudo il file
    fclose(fp);
    
    /* Output of the original message */
	printf("\nOriginal message:\n%s\n",msg.pt);
    
    //Calcolo della dimensione della stringa
    msg.pt_len = strlen(msg.pt)+1;
    
    //Calcolo l'hash e lo concateno al plain text
    hash_sha1(&msg,&msg.pt[msg.pt_len]);
    
    //Aggiorno la dimesione del plain text
    msg.pt_len += digest_len;

    //Rilascio dello spazio non utilizzato nel plaintext
    realloc(msg.pt, (size_t)msg.pt_len);
	
    //Cifratura del messaggio + hash
    my_encrypt(&msg);
    
    //Invio file
    send_and_close(msg.ct, msg.ct_len, "127.0.0.1", 1234);

    
    //Deallocazione e chiusura app
	free(msg.pt);
    free(msg.ct);
    
    return 0;
}