//
//  lib.h
//  sec-lab2
//
//  Created by Alessio Sanfratello on 22/03/14.
//
//

#ifndef sec_lab2_lib_h
#define sec_lab2_lib_h

#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdint.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <openssl/evp.h>
#include <openssl/rand.h>
#define SA struct sockaddr

struct crypto {
    char *pt;
    char *ct;
    char k[8];
    uint32_t pt_len;
    uint32_t ct_len;
};

void printbyte(char);
void my_encrypt(struct crypto *msg);
void my_decrypt(struct crypto *msg);
void hash_sha1(struct crypto *msg, char digest[]);
void send_and_close(char *msg, uint32_t msg_len, char *ip, unsigned int porta);
void enterPassword(char dest[]);

#endif
