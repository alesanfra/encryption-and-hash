//
//  server.c
//  sec-lab2
//
//  Created by Alessio Sanfratello on 19/03/14.
//
//

#include "lib.h"


int
answerToClient(int sk)
{
    FILE *fp = NULL;
    struct crypto msg;
    int ret=-1, i=0;
    int digest_len = 20;
    char digest[digest_len], mydigest[digest_len];
	
    //Inserisco la chiave nella struttura
    //stpncpy(msg.k,"ciaoluca",8);
    enterPassword(msg.k);
    
    //arriva la dimensione
    ret = recv(sk, (void *) &msg.ct_len, sizeof(uint32_t),0);
	if(ret == -1)
	{
		perror("\nErrore ricezione pacchetto");
		return -1;
	}
    
	printf("\nDimensione del ciphertext: %u\n\n",msg.ct_len);
    
    
    //alloco lo spazio per il cipher text
    msg.ct = (char*) calloc(msg.ct_len,sizeof(char));
    
    
	//ricezione del ciphertext
	ret = recv(sk, (void *) msg.ct, msg.ct_len,MSG_WAITALL);
	if(ret == -1)
	{
		perror("\nErrore ricezione pacchetto");
		return -1;
	}
    
    //decifro il messaggio
    my_decrypt(&msg);
    
    //EVP_CIPHER_CTX_cleanup(ctx);
    
	// salvo il digest ricevuto
	memcpy(digest,&msg.pt[msg.pt_len - digest_len],digest_len);
    msg.pt_len -= digest_len;
    //realloc(msg.pt,msg.pt_len);
    
    
    //Stampo il digest ricevuto
    printf("\nDigest SHA-1 ricevuto con il messaggio:\n");
	
	for (i = 0; i < digest_len ; i++)
		printbyte(digest[i]);
	printf("\n\n");
    
    // calcolo il digest sul testo ricevuto
	hash_sha1(&msg,mydigest);

    //Confronto i digest
	ret = memcmp(digest,mydigest,digest_len);
    
	if(ret == 0)
    {
		printf("Message verified!\n\n");
        
        //Stampo il messaggio a video
        printf("\nPlain text:\n%s\n\n", msg.pt);
        
        //Salvo il messaggio nel file "received-data.txt"
        fp = fopen("received-data.txt", "w");
        if (fp == NULL)
        {
            perror("Errore nell' apertura del File\n");
            exit(EXIT_FAILURE);
        }
        
        fprintf(fp, "%s", msg.pt);
        fclose(fp);
	}
	else
		printf("Message NOT verified!\n\n");
    
    

	
	return 0;
}



int
main(void)
{
	//allocazione delle strutture dati necessarie
	struct sockaddr_in my_addr, cl_addr;
	int ret, len, sk, cn_sk;
	
	//Verbose
	printf("Avvio del server\n");
	
	//creazione del socket di ascolto
	sk = socket(AF_INET, SOCK_STREAM, 0);
	
	if(sk == -1)
	{
		perror("Errore nella creazione del socket");
		return 0;
	}
	else
		printf("Socket di ascolto creato correttamente\n");
	
	//inizializzazione delle strutture dati
	memset(&my_addr, 0, sizeof(my_addr));
	my_addr.sin_family = AF_INET;
	my_addr.sin_addr.s_addr = htonl(INADDR_ANY);
	my_addr.sin_port = htons(1234);
	
	//bind del socket
	ret = bind(sk, (SA *) &my_addr, sizeof(my_addr));
	
	if(ret == -1)
	{
		perror("Errore nell'esecuzione della bind del socket");
		return 0;
	}
	else
		printf("Bind del socket eseguita correttamente\n");
    
	//messa in ascolto del server sul socket
	ret = listen(sk, 10);
	
	if(ret == -1)
	{
		perror("Errore nella messa in ascolto del server");
		return 0;
	}
	else
		printf("Server messo in ascolto sul socket correttamente\n");
	
	//dimensioni della struttura dove viene salvato l'ind del client
	len = sizeof(cl_addr);
	
	//ciclo in cui il server accetta connessioni in ingresso e le gestisce
	for(;;)
	{
        printf("\nWaiting for client message...\n\n");
        
		//accettazione delle connessioni ingresso
		cn_sk = accept(sk, (SA *) &cl_addr, (socklen_t *) &len);
		
		if(cn_sk == -1)
		{
			perror("Errore nell'accettazione di una richiesta");
			return 0;
		}
		else
			printf("Richiesta accettata correttamente\n");
		
		//gestione delle richieste
		answerToClient(cn_sk);
	}
	
	//Chiusura del socket di ascolto
	//Questo codice non verrà mai eseguito dato che si trova
	//dopo un ciclo infinito
	
	printf("\nChiusura del server\n");
	close(sk);
	return 0;
}



